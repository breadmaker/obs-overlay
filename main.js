function initSocket() {
    // La política de mismo-dominio no permite conectar desde localhost
    // (esta configuración se puede cambiar, pero es responsabilidad del
    // mantenedor del servidor :( )
    /*var socket = io.connect("http://flows.liquidsoap.fm", {
        transports: ["websocket", "htmlfile", "xhr-polling", "jsonp-polling"]
    });
    var firstConnect = true;
    socket.emit('join', "e710da7b9e83debd5dcb4a5455e9998caba8fca7");
    socket.on('joined', function (data) {
        if (!firstConnect) getInfo({
            "artist": data.artist,
            "title": data.title
        });
    });
    socket.on('connect_error', function (data) {
        console.debug("Ha ocurrido un error con la conexión.");
        if (manualFetchTimer === undefined) {
            manualFetchTimer = setInterval(function () {
                socket = io.connect(
                    "http://flows.liquidsoap.fm", {
                        transports: ["websocket", "htmlfile",
                            "xhr-polling", "jsonp-polling"
                        ]
                    });
                getInfo();
            }, 30000);
        }
    });
    socket.on('disconnect', function () {
        console.debug("Se ha perdido la conexión con el socket.");
    });
    socket.on('reconnecting', function () {
        console.debug("Se ha iniciado la reconexión con el socket.");
    });
    socket.on('reconnect', function () {
        console.debug("La reconexión con el socket ha sido exitosa.");
        firstConnect = false;
        socket.emit('join', "e710da7b9e83debd5dcb4a5455e9998caba8fca7");
    });
    socket.on('reconnect_failed', function () {
        console.debug("La reconexión con el socket ha fallado.");
    });
    socket.on("e710da7b9e83debd5dcb4a5455e9998caba8fca7", function (data) {
        if (data.cmd === "metadata") {
            getInfo({
                "artist": data.radio.artist,
                "title": data.radio.title
            });
            console.debug(
                "Se han recibidos nuevos metadatos desde el servidor.");
        } else if (data.cmd === "ping radio") {
            console.debug(
                "Se he recibido una solicitud de ping desde el servidor.");
        }
    });*/
}

var currentMetadata = {
    artist: "",
    title: ""
};

function getInfo(metadata) {
    if (metadata !== undefined) {
        if (currentMetadata.artist !== metadata.artist || currentMetadata.title !==
            metadata.title) {
            console.debug("Se han recibidos nuevos metadatos desde el servidor",
                metadata);
            currentMetadata = metadata;
            $("#wid-now-playing-loading").fadeIn(500);
            $.getJSON("https://api.radiognu.org/?no_cover", metadata, function (
                data) {
                $("#wid-now-playing-loading").fadeOut(500);
                if (data.artist === null && data.title === null)
                    showIcecastError();
                else processDataFromAPI(data);
            });
        } else {
            console.debug("Se han recibidos metadatos duplicados desde el servidor",
                metadata);
        }
    } else {
        $.getJSON("https://api.radiognu.org/?no_cover", function (data) {
            $("#wid-now-playing-loading").fadeOut(500);
            if (currentMetadata.artist != data.artist || currentMetadata.title !=
                data.title) {
                processDataFromAPI(data);
                currentMetadata = data;
            } else if (data.artist === null && data.title === null)
                showIcecastError();
        });
    }
}

function showIcecastError() {
    console.error("ICECASTERROR");
}

function processDataFromAPI(data) {
    $("#now-playing-container").addClass("in");
    $("#now-playing").removeClass("mini");
    setTimeout(function () {
        $("#now-playing").addClass("mini")
    }, 5000);
    if (!$("#cover")[0].hasAttribute("src") || $("#cover").attr("src") !==
        "https://api.radiognu.org/cover/" + data.album_id + ".png?img_size=120") {
        $("#cover").css("opacity", 0).attr("src", "https://api.radiognu.org/cover/" +
            data.album_id + ".png?img_size=120");
    }
    $("#title").text(data.title);
    $(".artist").text(data.artist);
    $(".album").text(data.album);
    $(".extra").text(data.country + " · " + data.year + (data.license
        .shortname !== undefined ? (" · " + data.license.shortname) : ""));
}

function initAudioVisualization() {
    var context = new(window.AudioContext || window.webkitAudioContext)();
    var analyser = context.createAnalyser();
    var radio = new Audio("");
    radio.crossOrigin = "anonymous";
    radio.preload = "auto";
    radio.autoplay = "autoplay";
    radio.src = "http://audio.radiognu.org/radiognu.ogg";
    var source = context.createMediaElementSource(radio);
    source.connect(analyser);
    source.connect(context.destination)
    //analyser.fftSize = 2048; // Por defecto es 2048
    /*var bufferLength = analyser.frequencyBinCount;
    var dataArray = new Uint8Array(bufferLength);
    analyser.getByteTimeDomainData(dataArray);

    var canvas = document.getElementById('now-playing-visualization');
    var canvasCtx = canvas.getContext("2d");
    var WIDTH = canvas.width,
        HEIGHT = canvas.height;
    canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

    function draw() {
        drawVisual = requestAnimationFrame(draw);
        analyser.getByteTimeDomainData(dataArray);
        canvasCtx.fillStyle = 'rgb(200, 200, 200)';
        canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
        canvasCtx.lineWidth = 2;
        canvasCtx.strokeStyle = 'rgb(0, 0, 0)';
        canvasCtx.beginPath();
        var sliceWidth = WIDTH * 1.0 / bufferLength;
        var x = 0;
        for (var i = 0; i < bufferLength; i++) {
            var v = dataArray[i] / 128.0;
            var y = v * HEIGHT / 2;
            if (i === 0) {
                canvasCtx.moveTo(x, y);
            } else {
                canvasCtx.lineTo(x, y);
            }
            x += sliceWidth;
        }
        canvasCtx.lineTo(canvas.width, canvas.height / 2);
        canvasCtx.stroke();
    };

    draw();*/
    analyser.fftSize = 128; // Por defecto es 2048
    var bufferLength = analyser.frequencyBinCount;
    var dataArray = new Uint8Array(bufferLength);
    analyser.getByteTimeDomainData(dataArray);

    var canvas = document.getElementById('now-playing-visualization');
    var canvasCtx = canvas.getContext("2d");
    var WIDTH = canvas.width,
        HEIGHT = canvas.height;
    canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);

    function draw() {
        drawVisual = requestAnimationFrame(draw);
        analyser.getByteFrequencyData(dataArray);
        canvasCtx.fillStyle = 'rgb(0, 0, 0)';
        canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
        var barWidth = (WIDTH / bufferLength) * 2.5;
        var barHeight;
        var x = 0;
        for (var i = 0; i < bufferLength; i++) {
            barHeight = dataArray[i];
            canvasCtx.fillStyle = 'rgb(50,' + (barHeight + 100) + ',50)';
            canvasCtx.fillRect(x, HEIGHT - barHeight / 2, barWidth, barHeight / 2);
            x += barWidth + 1;
        }
    };
    draw();
}

$(document).ready(function () {
    initAudioVisualization();
    getInfo();
    setInterval(function () {
        getInfo()
    }, 10000);
    $("#cover").on("load", function () {
        $("#cover").css("opacity", 1);
    });
});
